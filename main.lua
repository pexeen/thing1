function love.load()
	chars = {}
	timeReset = 18
	fontSize = 16
	time = 0
	font = love.graphics.newFont( "iosevka.ttf", fontSize )
	love.window.setFullscreen(true, "desktop")
	FirstRandom()
end

function love.update(dt)
	changes = 100 * math.sin(time) + 100
	for i=1, changes do
		chars[love.math.random(1,#chars)][love.math.random(1,#chars[1])] = RandomString(1)
	end
	time = time + dt
	if time > timeReset then
		randomizeColors()
		time = 0
	end
end

function love.draw()
	love.graphics.setFont(font)
	for i=1,#chars do
		for j=1,#chars[1] do
			love.graphics.setBackgroundColor(inv_red,inv_green,inv_blue)
			love.graphics.setColor(red,green,blue)
			love.graphics.print( chars[i][j], i*fontSize-fontSize, j*fontSize-fontSize)
		end
	end
end

function love.keypressed(key)
	
	if key == 'escape' then
		love.event.quit()
	end

	if key == 'space' then
		randomizeColors()
	end
end

function RandomString(length)
	length = length or 1
	if length < 1 then return nil end
	local array = {}
	for i = 1, length do
		array[i] = string.char(math.random(32, 126))
	end
	return table.concat(array)
end

function FirstRandom()
	for i=1,  love.graphics.getWidth()/fontSize do
		chars[i] = {}
		for j=1, love.graphics.getHeight()/fontSize do
			chars[i][j] = RandomString(1)
		end
	end
	randomizeColors()
end

function randomizeColors()
	red, green, blue = love.math.random(1,255), love.math.random(1,255), love.math.random(1,255)
	inv_red, inv_green, inv_blue = 255-red, 255-green, 255-blue
end
